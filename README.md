**About HiMama ClassRoom Project**


* This project is implemented as an assignment where it has two screens - *

	1. Class Room - Diplay the list of classroom for a center.

	2. Children List - Display the list of children inside the selected Classroom.

	

* Actions --> Children List screen has two actions - *

	1. CheckedIn/Checkedout - That update the status of the selected children.

	2. Switch Class - That switch the children from current class to another class. After successfully switching the class, the checkedIn status will be false automatically for the new assigned classroom.



# You can find the signed android apk inside the path --> android/app/release/HiMama_classroom.apk



## Tech Stack 

	1. React Navigation

	2. Redux

	3. Redux-Thunk

	4. Axios

	5. Radio button widget

## Start & Run the application on Android & iOS


Next, you�ll add a new file to this repository.

1. yarn install


* To start the server

 yarn start


* To run on iOS

--> First Time - yarn install && cd ios && pod deintegrate && pod install 
--> yarn ios


## Clone a repository

* To run on Android
--> yarn android

