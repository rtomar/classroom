import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  FlatList,
  Image,
  Switch,
  TouchableHighlight,
  Modal,
  Pressable,
  Alert,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchStudents,
  updateChildren,
  updateChildrenOnReduxStore,
} from "../../redux/actions/ChildrenAction";
import Style from "./styles/ChildrenListStyle";
import { Colors } from "../../theme";
import RadioForm from "react-native-simple-radio-button";
import LoaderView from "../../components/Loader";

const ChildrenListScreen = (props) => {
  // Navigation
  const navigation = props.navigation;
  const classroom = props.route.params.classroom;

  // Redux
  const dispatch = useDispatch();
  const { loading, childrens, error } = useSelector((state) => state.childrens);
  const { classrooms } = useSelector((state) => state.classRooms);

  // state
  const [showClassSwitchModal, setClassSwitchModalVisibility] = useState(false);
  const [childrenForSwitch, setChildren] = useState(null);

  useEffect(() => {
    // set Header Title
    navigation.setOptions({
      title: classroom?.name,
    });
    // fetch all Students for Selected ClassRooms
    dispatch(fetchStudents(classroom?.id));
  }, []);

  const toggleCheckedIn = (children, index) => {
    // update checkedIn Status
    // dispatch(updateChildrenOnReduxStore(childrens))
    children.checked_in = children.checked_in === "0" ? 1 : 0;
    dispatch(updateChildren(children));
    setTimeout(() => {
      dispatch(fetchStudents(classroom?.id));
    }, 1000);
  };

  const onSwitchClassRoom = (children) => {
    // check if has permission to switch
    if (classroom?.allClassroomsAccessible === "1") {
      setChildren(children);
      setClassSwitchModalVisibility(true);
    } else {
      Alert.alert("You don't have permission to switch");
    }
  };

  const onSubmitSwitchClass = (children, classroomToSwitch) => {
    setClassSwitchModalVisibility(!showClassSwitchModal);
    // as moving to children to outher class
    // changing the checked in status
    children.checked_in = "0";
    children.class_id = classroomToSwitch?.id;
    dispatch(updateChildren(children));
    // fetch all Students for Selected ClassRooms
    setTimeout(() => {
      dispatch(fetchStudents(classroom?.id));
    }, 500);
  };

  const renderSwitchClassModal = (children) => {
    // modify classroom data structure
    const classroomsData = classrooms
      .filter((data) => {
        return data.id !== classroom?.id;
      })
      .map((classroom) => {
        return { label: classroom.name, value: classroom };
      });
    let choosedClassroom = classroomsData[0].value;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={showClassSwitchModal}
        onRequestClose={() => {
          setClassSwitchModalVisibility(!showClassSwitchModal);
        }}
      >
        <View style={Style.centeredView}>
          <View style={Style.modalView}>
            <Text style={Style.title}>Switch between classrooms</Text>
            <View style={Style.radioForm}>
              <RadioForm
                radio_props={classroomsData}
                initial={0}
                formHorizontal={false}
                labelHorizontal={true}
                buttonColor={Colors.blue}
                animation={true}
                onPress={(value) => {
                  choosedClassroom = value;
                }}
              />
            </View>
            <View style={Style.actionContainer}>
              <Pressable
                style={[Style.button, Style.buttonClose]}
                onPress={() => {
                  onSubmitSwitchClass(children, choosedClassroom);
                }}
              >
                <Text style={Style.textStyle}>Submit</Text>
              </Pressable>
              <Pressable
                style={[Style.button, Style.buttonClose]}
                onPress={() =>
                  setClassSwitchModalVisibility(!showClassSwitchModal)
                }
              >
                <Text style={Style.textStyle}>Cancel</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    );
  };

  const renderChildrenListItem = (children, index) => {
    return (
      <View>
        {children && (
          <View style={Style.itemContainer}>
            <Text style={Style.title}>{children.fullName}</Text>
            <View style={Style.actionContainer}>
              <View style={Style.widget}>
                <Switch
                  trackColor={{ false: Colors.black, true: Colors.pink }}
                  thumbColor={
                    children.checked_in === "1" ? Colors.yellow : Colors.white
                  }
                  ios_backgroundColor={Colors.blue}
                  onValueChange={() => toggleCheckedIn(children, index)}
                  value={children.checked_in === "1"}
                />
              </View>
              <TouchableHighlight
                style={Style.widget}
                onPress={() => onSwitchClassRoom(children)}
              >
                <Image
                  style={Style.switchImage}
                  source={require("../../assets/images/login_black.png")}
                />
              </TouchableHighlight>
            </View>
          </View>
        )}
      </View>
    );
  };

  const renderFlatList = () => {
    return (
      <View>
        {showClassSwitchModal && renderSwitchClassModal(childrenForSwitch)}
        <FlatList
          data={childrens}
          renderItem={({ item, index }) => renderChildrenListItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  const renderSectionList = () => {};

  return (
    <View style={Style.mainContainer}>
      {classroom ? renderFlatList() : renderSectionList()}
      <LoaderView isLoading={loading} />
    </View>
  );
};

export default ChildrenListScreen;
