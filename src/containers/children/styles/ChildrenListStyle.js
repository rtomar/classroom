import {StyleSheet} from 'react-native';
import {Colors, Metrics} from '../../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.white,
    marginTop: Metrics.smallMargin,
  },
  itemContainer: {
    backgroundColor: Colors.blue,
    marginRight: Metrics.smallMargin,
    marginBottom: Metrics.smallMargin,
    marginLeft: Metrics.smallMargin,
    borderRadius: 4,
    height: 80,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'space-between',
    borderColor: Colors.transparent,
  },
  radioForm:{
    margin:Metrics.baseMargin
  },
  actionContainer:{
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'space-between'
  },
  title: {
    color: Colors.black,
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.smallMargin,
    fontWeight: 'bold',
  },
  switchImage: {
    width: Metrics.images.medium,
    height: Metrics.images.medium,
    marginRight: Metrics.baseMargin,
    padding: Metrics.smallMargin
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 10,
    padding: Metrics.baseMargin,
    elevation: 2,
    margin: Metrics.smallMargin
  },
  buttonClose: {
    backgroundColor: Colors.pink,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  noChildrenTitle: {
    color: Colors.black,
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.smallMargin,
    fontWeight: 'bold',
  },
  widget:{
    padding:Metrics.smallMargin
  }
});
