import React, {useEffect} from 'react';
import {Image, TouchableHighlight, Text, View, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {fetchAllClassRooms} from '../../redux/actions/ClassRoomActions';
import Style from './styles/ClassRoomStyle'
import LoaderView from '../../components/Loader';

const ClassRoomScreen = (props) => {
  // Redux
  const dispatch = useDispatch();
  const {loading, classrooms, error} = useSelector(state => state.classRooms);

  // Navigation
  const navigation = props.navigation

  useEffect(() => {
    // fetch all classrooms
    dispatch(fetchAllClassRooms());
  }, []);

  const onTapClassRoom = (classroom, index)=>{
    //navigate to Children Screen
    navigation.navigate('Childrens', { classroom: classroom });
  }

  const renderClassRoomsItem = (classroom, index) => {
    return (
      <TouchableHighlight  onPress={() => onTapClassRoom(classroom, index)}>
        { classroom && 
          <View style={Style.itemContainer}>
            <Text style={Style.title}>{classroom.name}</Text>
            <Image style={Style.arrowImage}  source={require('../../assets/images/navigate_next_black.png')}/>
          </View>
        }
      </TouchableHighlight>
    );
  }

  return (
    <View style={Style.mainContainer}>
        <FlatList
          data={classrooms}
          renderItem={({item, index}) => renderClassRoomsItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
        <LoaderView isLoading={loading} />
    </View>
  );
};

export default ClassRoomScreen;
