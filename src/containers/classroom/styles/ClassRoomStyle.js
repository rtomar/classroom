import {StyleSheet} from 'react-native';
import {Colors, Metrics} from '../../../theme';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.white,
    marginTop: Metrics.smallMargin,
  },
  itemContainer: {
    backgroundColor: Colors.blue,
    marginRight: Metrics.smallMargin,
    marginBottom: Metrics.smallMargin,
    marginLeft: Metrics.smallMargin,
    borderRadius: 4,
    height: 80,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'space-between',
    borderColor: Colors.transparent,
  },
  title: {
    color: Colors.black,
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.smallMargin,
    fontWeight: 'bold',
  },
  arrowImage: {
    width: Metrics.images.medium,
    height: Metrics.images.medium,
    marginRight: Metrics.baseMargin
  }
});
