
const colors = {

    header: '',
    textColor: '#2d2d2d',
    yellow: '#FFED1A',
    blue: '#12CDD4',
    pink: '#FF6EB0',
    charcoal: '#595959',
    coal: '#2d2d2d',
    black: '#000',
    white: 'white',
    red: '#E64044',
    green: '#008000',
    white: 'white',
    transparent: 'rgba(0,0,0,0)',
}
export default colors