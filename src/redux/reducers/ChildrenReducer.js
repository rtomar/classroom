import {
  FETCH_CLASS_STUDENTS,
  UPDATE_STUDENT,
  STUDENT_HTTP_ERROR,
  STUDENT_UPDATE_HTTP_ERROR,
  SHOW_LOADING_STUDENT,
  FETCH_ALL_STUDENTS,
  SHOW_LOADING_UPDATE_STUDENT,
} from '../actions/ActionType';

const INITIAL_STATE = {
  loading: false,
  childrens: [],
  error: '',
};

const ChildrenReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SHOW_LOADING_STUDENT:
    case SHOW_LOADING_UPDATE_STUDENT:
      return {...state, loading: action.payload};
    case FETCH_CLASS_STUDENTS:
    case FETCH_ALL_STUDENTS:
      return {...state, childrens: action.payload, loading: false};
    case UPDATE_STUDENT:
      return {...state, loading: false};
    case STUDENT_HTTP_ERROR:
    case STUDENT_UPDATE_HTTP_ERROR:
      return {...state, loading: false, error: action.payload};
    default:
      return state;
  }
};

export default ChildrenReducer;
