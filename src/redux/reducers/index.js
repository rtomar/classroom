import {combineReducers} from 'redux'
import ClassRoomReducer from './ClassRoomReducer'
import ChildrenReducer from './ChildrenReducer'

export default combineReducers({
    classRooms: ClassRoomReducer,
    childrens: ChildrenReducer
})