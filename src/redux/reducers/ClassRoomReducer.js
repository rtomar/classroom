import {
  FETCH_ALL_CLASSROOMS,
  CLASSROOM_HTTP_ERROR,
  SHOW_LOADING_CLASSROOM,
} from '../actions/ActionType';

const INITIAL_STATE = {
  loading: false,
  classrooms: [],
  error: '',
};

const ClassRoomReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SHOW_LOADING_CLASSROOM:
      return ({...state, loading: action.payload});
    case FETCH_ALL_CLASSROOMS:
      return ({...state, classrooms: action.payload, loading: false});
    case CLASSROOM_HTTP_ERROR:
      return ({...state, loading: false, error: action.payload});
    default:
      return state;
  }
};

export default ClassRoomReducer;
