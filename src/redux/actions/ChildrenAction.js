import {
  FETCH_CLASS_STUDENTS,
  UPDATE_STUDENT,
  STUDENT_HTTP_ERROR,
  STUDENT_UPDATE_HTTP_ERROR,
  SHOW_LOADING_STUDENT,
  FETCH_ALL_STUDENTS,
  SHOW_LOADING_UPDATE_STUDENT
} from './ActionType';
import axios from 'axios';
import {CHILDREN_API_ENDPOINT} from '../../services/Url';

export const fetchStudents = classRoomId => {
  return dispatch => {
    dispatchActions(dispatch, SHOW_LOADING_STUDENT, true);
    // fetch all Students
    let url = `${CHILDREN_API_ENDPOINT}?all`;
    if (classRoomId) {
      url = `${url}&classroomId=${classRoomId}`;
    }
    const axiosClient = axios.create({
      headers: {
        Accept: 'application/json',
      },
    });
    axiosClient({
      method: 'get',
      url: url,
    })
      .then(response => {
        dispatchActions(
          dispatch,
          classRoomId ? FETCH_CLASS_STUDENTS : FETCH_ALL_STUDENTS,
          response?.data?.response?.result,
        );
      })
      .catch(error => {
        dispatchActions(dispatch, STUDENT_HTTP_ERROR, error);
      });
  };
};

export const updateChildren = children => {
  return dispatch => {
    dispatchActions(dispatch, SHOW_LOADING_UPDATE_STUDENT, true);
    // fetch all Students
    const url = `${CHILDREN_API_ENDPOINT}?update`;
    axios.post(url, {...children, checked_in: parseInt(children.checked_in)})
      .then(response => {
        dispatchActions(
          dispatch,
          UPDATE_STUDENT,
          response?.data?.response?.result,
        );
      })
      .catch(error => {
        dispatchActions(dispatch, STUDENT_UPDATE_HTTP_ERROR, error);
      });
  };
};

export const updateChildrenOnReduxStore = (childrens,children) => {
  return dispatch => {
    dispatchActions(dispatch, SHOW_LOADING_UPDATE_STUDENT, true);
    // find the children in childrens and update the class_id
    const classChildrens = childrens.map((child)=>{
       if(child.id === children.id){
         child.checked_in = children.checked_in
       }
       return child
    })
    dispatchActions(
      dispatch,
      UPDATE_STUDENT,
      classChildrens
    )
  }
}

/*
 *
 * @param {*} dispatch -- dispatch the actions to the reducers
 * @param {*} type - type define the action and based on action reducter return the state
 * @param {*} payload - is the object that can pass with action type
 */
const dispatchActions = (dispatch, action, payload) => {
  dispatch({
    type: action,
    payload: payload,
  });
};
