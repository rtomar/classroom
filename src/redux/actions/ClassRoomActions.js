import {FETCH_ALL_CLASSROOMS,CLASSROOM_HTTP_ERROR,SHOW_LOADING_CLASSROOM} from './ActionType'
import {CLASSROOM_API_ENDPOINT} from '../../services/Url'
import axios from 'axios'

export const fetchAllClassRooms = ()=>{
    return (dispatch)=>{
        dispatchActions(dispatch,SHOW_LOADING_CLASSROOM,true)
        // fetch all classrooms
        const url = `${CLASSROOM_API_ENDPOINT}?all`
        const axiosClient = axios.create({
            headers: {
                Accept: 'application/json',
            }
        });
        axiosClient({
            method: 'get',
            url: url
        }).then(response => {
            dispatchActions(dispatch,FETCH_ALL_CLASSROOMS,response?.data?.response?.result)
        }).catch(error => {
            dispatchActions(dispatch,CLASSROOM_HTTP_ERROR,error)
        });
    }
}


/*
 * 
 * @param {*} dispatch -- dispatch the actions to the reducers
 * @param {*} type - type define the action and based on action reducter return the state 
 * @param {*} payload - is the object that can pass with action type
 */
const dispatchActions = (dispatch, action, payload) => {
    dispatch({
        type: action,
        payload: payload
    })
}