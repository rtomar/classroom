import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import {Colors} from '../theme'
import ClassRoomScreen from '../containers/classroom/ClassRoomScreen'
import ChildrenListScreen from '../containers/children/ChildrenListScreen'

const Stack = createNativeStackNavigator();

function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="ClassRooms" screenOptions={{
        headerStyle: {
          backgroundColor: Colors.yellow,
        },
        headerTintColor: Colors.black,
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
      >
        <Stack.Screen name="ClassRooms"
          component={ClassRoomScreen}
          options={{ title: 'ClassRooms', headerTitleAlign:'center'}}
        />
        <Stack.Screen name="Childrens"
          component={ChildrenListScreen}
          options={{ title: 'Children', headerTitleAlign:'center'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigation;